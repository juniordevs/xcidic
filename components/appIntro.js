import React from 'react';
import { StyleSheet, View, Alert, Text, AsyncStorage } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

const styles = StyleSheet.create({
    buttonCircle: {
        width: 50,
        height: 50,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        fontSize: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 320,
        height: 320,
    },
});


const slides = [
    {
        key: 'firtsIntro',
        title: 'A Very Simple News App',
        text: 'Have you ever wanted to read latest news in your app?',
        
        backgroundColor: 'orange',
    },
    {
        key: 'secondIntro',
        title: 'Access All News',
        text: 'Very easy, read latest news at the comfort of your own home',
        
        backgroundColor: 'orange',
    },
    {
        key: 'endIntro',
        title: 'Start Now and Keep Updated',
        text: 'So, what are you waiting for?',
        

        backgroundColor: 'orange',
    }
];


export default class App extends React.Component {
    _renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Text>Next</Text>
            </View>
        );
    };
    doneButton = async () => {

        await this.props.navigation.navigate('Login');
        await AsyncStorage.setItem("introVisible", "false");
    };
    UNSAFE_componentWillMount = async () => {
        const result = await AsyncStorage.getItem("introVisible")
        if (result === "false") {
            await this.props.navigation.navigate('Login');
        }
    }
    render() {
        return (
            <AppIntroSlider
                slides={slides}
                doneLabel={"Finish"}
                nextLabel={"Next"}
                prevLabel={"Previous"}
                showNextButton
                showSkipButton
                showPrevButton
                onSkip={() => this.props.navigation.navigate('Login')}
                skipLabel={"Skip"}
                onDone={this.doneButton}
            />
        );
    }
}