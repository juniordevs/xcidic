/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    TextInput,
    Text,
    AsyncStorage,
    Alert,
    ProgressBarAndroid,
    SectionList,
    StatusBar,
    navigator
} from 'react-native';
import { Container, Header, Title, ActionSheet, Button, Icon, Left, Right, Body, Item, Picker, Content } from "native-base";
import {
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class CategoryNewsHeader extends Component {
    static navigationOptions = {

        drawerLabel: 'Notifications',
        drawerIcon: ({ tintColor }) => (
            <Image
                source="https://i.dlpng.com/static/png/1504875-sports-png-vector-royalty-free-stock-sports-png-420_420_preview.png"
                style={[styles.icon, { tintColor: tintColor }]}
            />
        ),
    };
    state = {
        clicked: undefined,
    }
    onNewsView = () => {
        const { news } = this.props;
        const dizi = []
        news.map(Item => dizi.push(Item.title))
        Alert.alert(dizi[0]);
    }

    render() {
        const { title, openMenu } = this.props;
        return (
            <Header style={{ backgroundColor: "orange" }}>
                <Left>
                    <Button transparent onPress={openMenu}>
                        <Icon name='menu' />
                    </Button>
                </Left>
                <Body>
                    <Title> {title}</Title>
                </Body>
                <Right>
                    <Button transparent onPress={() => Alert.alert("Refresh")}>
                        <Icon name='refresh' />
                    </Button>

                </Right>
            </Header>
        );
    }
}

export default CategoryNewsHeader;
