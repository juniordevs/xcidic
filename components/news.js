/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    TextInput,
    Text,
    Alert,
    ProgressBarAndroid,
    AsyncStorage,
    SectionList,
    StatusBar,
    navigator,
    Dimensions
} from 'react-native';
import { DrawerNavigator, createDrawerNavigator, DrawerItems, createAppContainer, createStackNavigator } from "react-navigation"
import { Container, List, ListItem, Spinner, Header, Title, Button, Icon, Left, Right, Body } from "native-base";
import { Paragraph, Menu, Divider, Provider } from 'react-native-paper';
import {
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { } from "react-native-elements"
import firebase from "firebase"
import NewsHeader from "./homeHeaders";
import axios from "axios";
import NewsCard from "./newsCard";
import NewsModal from "./newsModal"
import actionSheet from "../helpers/actionSheet";

import SearchResults from './SearchResults/SearchResults';


class News extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: {},
            news: [],
            isLoading: false,
            modalVisible: false,
            selectedNews: {},
            visible: false
        }
    }
    static navigationOptions = {
        drawer: () => ({
            label: 'Home',
            icon: ({ tintColor }) => (
                <Icon
                    name="home"
                    color={tintColor}
                />
            ),
        }),
    }



    refreshMethod = async () => {
        await this.setState({ isLoading: false })
        const news = await axios.get("https://express-news-api.herokuapp.com/v1/newss")
        await this.setState({ news: news.data });
        return await this.setState({ isLoading: true })
    }

    async getCurrentUser() {
        const result = firebase.auth().currentUser;
        const storage = await AsyncStorage.getItem("currentUser")
        const item = JSON.parse(storage)
        return this.setState({ currentUser: item })
    }

    async getNews() {
        const news = await axios.get("https://express-news-api.herokuapp.com/v1/newss")
        await this.setState({ news: news.data });
        return await this.setState({ isLoading: true })
    }
    onModalView = (title, description, content, imageUrl, sourceName, url) => {
        const newsItem = {
            title: title,
            description: description,
            content: content,
            imageUrl: imageUrl,
            sourceName: sourceName,
            url: url
        }

        this.setState({ modalVisible: true, selectedNews: newsItem });
    }

    componentDidMount = async () => {
        this.getCurrentUser();
        this.getNews();
    }

    onCancelModal = () => {
        this.setState({ modalVisible: false, selectedNews: {} });
    }

    onOpenMenu = () => {
        this.props.navigation.openDrawer();
    }
    _closeMenu = () => { this.setState({ visible: false }) };

    render() {
        const { news, isLoading } = this.state;
        return (

            <View>

                <NewsHeader currentUser={this.state.currentUser}
                    openMenu={this.onOpenMenu}
                    refresh={this.refreshMethod} news={this.state.news} />
                <NewsModal
                    onCancel={this.onCancelModal}
                    news={this.state.selectedNews}
                    visibilty={this.state.modalVisible}
                />
                
                <ScrollView>
                
                    {
                        !isLoading ? (<Spinner style={{ justifyContent: "center", alignContent: "center" }} color='blue' />)
                            : (
                                <View>
                                    <SearchResults />
                                <List>
                                    {
                                        news.map(item => (
                                            <ListItem >
                                                <NewsCard
                                                    title={item.title}
                                                    description={item.description}
                                                    imageUrl={item.imageUrl}
                                                    sourceName={item.source.name}
                                                    content={item.content}
                                                    onOpenModal={this.onModalView}
                                                    url={item.url}
                                                />
                                            </ListItem>
                                        ))
                                    }
                                </List>
                                </View>
                            )
                    }
                </ScrollView>
            </View>

        );
    }
}

export default News;
