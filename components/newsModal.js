import React, { Fragment, Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  Text,
  Modal,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  ProgressBarAndroid,
  Linking,
  SectionList,
  AsyncStorage,
  StatusBar,
  ImageBackground,
  navigator,
  Image

} from 'react-native';
import {

  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Movies from './news'
import { SwitchActions } from 'react-navigation';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import firebase from "firebase"
import { Container, List, ListItem, Spinner, Header, Title, Button, Icon, Left, Right, Body } from "native-base";

class NewsModal extends Component {
  state = {
    modalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  openNewsLink = () => {
    Linking.openURL(this.props.news.url);
  }

  render() {
    const { news, visibilty, onCancel } = this.props;
    const image = news.imageUrl
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={visibilty}
        onRequestClose={onCancel}>
        <ScrollView>
          <View style={{ marginTop: 22 }}>
            <Text style={{ fontSize: 25, marginLeft: 12, marginTop: 10, color: "#5FA6C4" }}>{news.title}</Text>
            <View>
              <Image source={{ uri: image }} style={{ height: 200, width: "95%", margin: 12 }} />
            </View>
            <Text style={{ fontSize: 18, margin: 12 }} numberOfLines={50}>{news.description}</Text>
            <Text style={{ fontSize: 13, margin: 12, color: "blue" }} onPress={this.openNewsLink} numberOfLines={50}>Read more ...</Text>
          </View>
        </ScrollView>
        <Button style={{ width: "100%", backgroundColor: "#5499C7", justifyContent: "center", height: 50, marginBottom: 0, bottom: 0, position: "relative" }} icon light onPress={onCancel}>
          <Icon name='arrow-back' />
        </Button>
      </Modal>
    );
  }
}

export default NewsModal