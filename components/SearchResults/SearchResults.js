import React, {Component} from 'react';
import {Alert, Linking, Text, TouchableOpacity, View, Image, ScrollView, FlatList} from 'react-native';
import NewsCard from "../newsCard";
import {connect} from 'react-redux';
import {Body, Card, CardItem, List, ListItem } from 'native-base';
const styles = require('./SearchResultsStyles');

class SearchResults extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log("RESULTS SEARCH ", this.props.results);
    console.log("LENGTH",this.props.results.length)
    return (
      
            // <List>
            //                     {
            //                             this.props.results.map(item => (
            //                                 <ListItem >
            //                                     <NewsCard
            //                                         title={item.title}
            //                                         description={item.description}
            //                                         imageUrl={item.imageUrl}
            //                                         sourceName={item.source.name}
            //                                         content={item.content}
            //                                         onOpenModal={this.onModalView}
            //                                         url={item.url}
            //                                     />
            //                                 </ListItem>
            //                             ))
            //                         }
            //                     </List>
            <View>
               <List>
                                    {   this.props.results !== 'News not found!' ? (
                                        this.props.results.map(item => (
                                            <ListItem >
                                               <Card style={{ width: "100%" }} >
                <CardItem header bordered>
                    <Text>{item.title}</Text>
                </CardItem>
                <CardItem>
                    <Body>
                        <Image source={{ uri: item.imageUrl }} style={{ height: 200, width: "100%", flex: 1 }} />
                    </Body>
                </CardItem>
                <CardItem bordered>
                    <Body>
                        <Text numberOfLines={3}>
                            {item.description}
                        </Text>
                    </Body>
                </CardItem>
                <CardItem footer bordered>
                    <Text>{item.source.name}</Text>
                </CardItem>
            </Card>
                                            </ListItem>
                                        ))) : Alert.alert("Sorry, your search is not found in database")
                                    }
                                </List>
             
            </View>
           
                
      
    );
  }
}

function mapStateToProps(state) {
  return {
    results: state.results,
  };
}

export default connect(mapStateToProps, null)(SearchResults);
