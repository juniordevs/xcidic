/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    TextInput,
    Modal,
    Alert,
    ProgressBarAndroid,
    SectionList,
    StatusBar,
    navigator,
    Image
} from 'react-native';
import { Container, Header, Text, Title, Button, Icon, Left, Card, CardItem, Right, Body } from "native-base";
import {
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import firebase from "firebase"
import NewsHeader from "./homeHeaders";
import axios from "axios";

class NewsCard extends Component {

    render() {
        const { title, url, content, description, imageUrl, sourceName, onOpenModal } = this.props;
        const image = imageUrl
        return (
            <Card style={{ width: "100%" }} onTouchEnd={onOpenModal.bind(this, title, description, content, imageUrl, sourceName, url)}>
                <CardItem header bordered>
                    <Text>{title}</Text>
                </CardItem>
                <CardItem>
                    <Body>
                        <Image source={{ uri: image }} style={{ height: 200, width: "100%", flex: 1 }} />
                    </Body>
                </CardItem>
                <CardItem bordered>
                    <Body>
                        <Text numberOfLines={3}>
                            {description}
                        </Text>
                    </Body>
                </CardItem>
                <CardItem footer bordered>
                    <Text>{sourceName}</Text>
                </CardItem>
            </Card>
        );
    }
}

export default NewsCard;
