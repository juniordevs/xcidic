/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Button,
    TextInput,
    Text,
    Image,
    Alert,
    TouchableOpacity,
    TouchableHighlight,
    ProgressBarAndroid,
    SectionList,
    AsyncStorage,
    StatusBar,
    ImageBackground,
    navigator,

} from 'react-native';
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Movies from './news'
import { SwitchActions } from 'react-navigation';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import firebase from "firebase"
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
        }
    }
    static navigationOptions = {
        title: 'Latest News App',
        headerStyle: {
            backgroundColor: '#000066',
        },
        headerTintColor: 'white',
        headerLeft: null,
    }
    onLogin = () => {
        const { email, password } = this.state;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(async (data) => {
                console.log("Firebase USER:", data.user);
                await this.props.navigation.navigate('Home');
                await AsyncStorage.setItem("currentUser", JSON.stringify(data));
            })
            .catch((error) => {
                const { code, message } = error;
                Alert.alert("Sorry, wrong password or email", message);
            });
    }
    UNSAFE_componentWillMount = async () => {
        const result = await AsyncStorage.getItem("currentUser")
        const item = JSON.parse(result);
        console.log("ITEM.USER", item.user);
        if (item.user.email !== "") {
            this.props.navigation.navigate('Home');
        } else {
            return 0;
        }
    }
    render() {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    padding: 30,
                    backgroundColor: "white"
                }}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        marginTop: 1
                    }}
                >
                    <Text style={{
                        textAlign: "center",
                        marginBottom: 25,
                        fontSize: 20,
                        color: "#000066"

                    }}>Welcome to News App</Text>
                    <View>

                        <TextInput
                            style={{
                                height: 50,
                                borderColor: '#808B96',
                                marginBottom: 7,
                                borderRadius: 12,
                                borderWidth: 1,
                                fontSize: 21,
                                backgroundColor: "white",
                                textAlign: "center"
                            }}
                            onChangeText={(Text) => this.setState({ email: Text })}
                            placeholder="Email" />
                    </View>
                    <View>
                        <TextInput
                            style={{
                                height: 50,
                                borderColor: '#808B96',
                                marginBottom: 7,
                                borderRadius: 12,
                                borderWidth: 1,
                                fontSize: 21,
                                backgroundColor: "white",
                                textAlign: "center"
                            }}
                            onChangeText={(text) => this.setState({ password: text })}
                            secureTextEntry={true}
                            placeholder="Password" />
                    </View>
                    <View>
                        <TouchableHighlight
                            onPress={this.onLogin}
                            style={{
                                backgroundColor: '#FF0033',
                                marginTop: 10,
                                padding: 10,
                                borderRadius: 12,
                                height: 50,
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                            <Text style={{
                                color: "white",
                                fontSize: 30,
                            }}>Sign In</Text>
                        </TouchableHighlight>
                        <Text style={{
                            textAlign: "center",
                            marginTop: 25,
                            fontSize: 20,
                            color: "#FF0033"

                        }} onPress={() => {
                            this.props.navigation.navigate('Register')
                        }}>Register a new account!</Text>
                    </View>

                </View>
            </View>
        );
    }
}

export default Login
