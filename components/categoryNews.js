/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    TextInput,
    Text,
    Image,
    Alert,
    TouchableHighlight,
    ProgressBarAndroid,
    SectionList,
    StatusBar,
    ImageBackground,
    navigator
} from 'react-native';
import { Container, List, ListItem, Spinner, Title, Button, Icon, Left, Right, Body } from "native-base";

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Movies from './news'
import { SwitchActions } from 'react-navigation';
import firebase from "firebase";
import NewsCard from "./newsCard";
import axios from "axios"
import NewsModal from "./newsModal"
import SearchResults from './SearchResults/SearchResults';

import CategoryHeader from "./categoryNewsHeader";
class SportNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: [],
            isLoading: false,
            category: "",
            modalVisible: false,
            selectedNews: {}
        }
    }

    onOpenMenu = () => {
        this.props.navigation.openDrawer();
    }

    async getNews() {
        await this.setState({ isLoading: false })
        const { navigation } = this.props;
        const category = await navigation.getParam('categoryName', 'NoCategory');
        const news = await axios.get(`https://express-news-api.herokuapp.com/v1/newss/search/keywords?q=`+category)
        console.log("GET NEWS", news.data);
        await this.setState({ news: news.data });
        return await this.setState({ isLoading: true })
    }
    componentDidMount = () => {
        this.getNews();
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.navigation.getParam('categoryName', 'NoCategory') !==
            this.props.navigation.getParam('categoryName', 'NoCategory')
        ) {

            this.getNews()
        }
    }
    onModalView = (title, description, content, imageUrl, sourceName, url) => {
        const newsItem = {
            title: title,
            description: description,
            content: content,
            imageUrl: imageUrl,
            sourceName: sourceName,
            url: url
        }

        this.setState({ modalVisible: true, selectedNews: newsItem });
    }
    onCancelModal = () => {
        this.setState({ modalVisible: false, selectedNews: {} });
    }

    render() {
        const { navigation } = this.props;
        const { news, isLoading } = this.state;
        return (
            <View>
                <CategoryHeader
                    title={navigation.getParam('headerTitle', 'NoTitle')}
                    openMenu={this.onOpenMenu}
                />
                <NewsModal
                    onCancel={this.onCancelModal}
                    news={this.state.selectedNews}
                    visibilty={this.state.modalVisible} />
                {/* <SearchResults /> */}
                
                <ScrollView>
                    {
                        !isLoading ? (<Spinner style={{ justifyContent: "center", alignContent: "center" }} color='blue' />)
                            : (
                                <List>
                                    {
                                        news.map(item => (
                                            <ListItem >
                                                <NewsCard
                                                    title={item.title}
                                                    description={item.description}
                                                    imageUrl={item.imageUrl}
                                                    sourceName={item.source.name}
                                                    content={item.content}
                                                    onOpenModal={this.onModalView}
                                                    url={item.url}
                                                />
                                            </ListItem>
                                        ))
                                    }
                                </List>
                            )
                    }
                </ScrollView>
            </View>
        );
    }
}

export default SportNews
