const React = require('react-native');
const {Dimensions, StyleSheet} = React;

module.exports = StyleSheet.create({
  searchBarContainer: {
    width: Dimensions.get('window').width - 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    borderBottomWidth: 0,
    marginVertical: 10,
    borderColor: 'lightgray',
    flex: 1,
  },
  textInputSearch: {
    flex: 7,
    borderColor: 'lightgray',
    borderWidth: 0,
    borderRadius: 5,
    marginRight: 10,
    height: 40,
    paddingLeft:0,
    
  },
  textSearchButton: {
    flex: 1,
    // backgroundColor: 'white',
    borderRadius: 5,
    alignItems: 'flex-end',
    justifyContent: 'center',
    padding: 10,
    height: 40,
    width:100
  },
});
