import React, {Component} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
// import {FontAwesome} from '@expo/vector-icons';
// import {API_KEY} from '../../constants/api.js';
import { Icon } from 'native-base';

import {connect} from 'react-redux';
import {searchResults} from '../../actions';

const styles = require('./SearchBarStyles');
const cx = '012040760514751621405:3qggxrt9tk8';
// const apiURL = 'https://www.googleapis.com/customsearch/v1';
const apiURL = 'https://express-news-api.herokuapp.com/v1/newss/search/';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
    };
  }
  searchOnMedium = () => {
    // let URL =
    //   apiURL + '?key=' + 'AIzaSyBJ2nkStsLfiv5n4sp0WfhVY2l--FG7m-0' + '&cx=' + cx + '&q=' + this.state.searchTerm;
    let URL = apiURL + 'keywords?q=' + this.state.searchTerm;
    fetch(URL, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        const contentType = response.headers.get("content-type");
        console.log("RESPONSE API", response);
        if (contentType && contentType.indexOf("application/json") !== -1) {
            return response.json().then(data => {
              // process your JSON data further
              console.log("DATA ", data);
              this.props.searchResults(data);
            });
          } else {
            return response.text().then(text => {
              // this is text, do something with it
                 console.log("TEXT DATA", text);
                this.props.searchResults(text)
            });
          }
      })
      .catch((error) => console.log("ERROR", error));
  };

  render() {
    return (
      <View style={styles.searchBarContainer}>
        <TextInput
          placeholder="Find your favorite news"
          style={styles.textInputSearch}
          underlineColorAndroid={'transparent'}
          onChangeText={(searchTerm) => this.setState({searchTerm})}
          value={this.state.searchTerm}
        />
        <TouchableOpacity
          style={styles.textSearchButton}
          onPress={() => this.searchOnMedium()}>
          {/* <FontAwesome name="search" size={16} color="#000" /> */}
          {/* <Text>Click Search</Text> */}
          <Icon type="FontAwesome" name="search" />
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(null, {searchResults})(SearchBar);
