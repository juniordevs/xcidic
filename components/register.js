/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Button,
    TextInput,
    Text,
    Image,
    Alert,
    TouchableHighlight,
    ProgressBarAndroid,
    SectionList,
    StatusBar,
    ImageBackground,
    navigator
} from 'react-native';
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Movies from './news'
import { SwitchActions } from 'react-navigation';
import firebase from "firebase";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            email: "",
            password: "",
        }
    }

    onRegister = () => {
        firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => this.props.navigation.navigate('Login'))
            .catch(error => Alert.alert(error.message))
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    padding: 30,
                    backgroundColor: "white"
                }}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        marginTop: 1
                    }}
                >
                      <Text style={{
                        textAlign: "center",
                        marginBottom: 25,
                        fontSize: 20,
                        color: "#000066"

                    }}>Create Your New Account</Text>
                    <View>
                  
                        <TextInput
                            style={{
                                height: 50,
                                borderColor: '#808B96',
                                marginBottom: 7,
                                borderRadius: 12,
                                borderWidth: 1,
                                fontSize: 21,
                                backgroundColor: "white",
                                textAlign: "center"
                            }}
                            onChangeText={(Text) => this.setState({ username: Text })}
                            placeholder="Enter your username" />
                    </View>
                    <View>
                        <TextInput
                            style={{
                                height: 50,
                                borderColor: '#808B96',
                                marginBottom: 7,
                                borderRadius: 12,
                                borderWidth: 1,
                                fontSize: 21,
                                backgroundColor: "white",
                                textAlign: "center"
                            }}
                            onChangeText={(Text) => this.setState({ email: Text })}
                            placeholder="Email" />
                    </View>
                    <View>
                        <TextInput
                            style={{
                                height: 50,
                                borderColor: '#808B96',
                                marginBottom: 7,
                                borderRadius: 12,
                                borderWidth: 1,
                                fontSize: 21,
                                backgroundColor: "white",
                                textAlign: "center"
                            }}
                            onChangeText={(text) => this.setState({ password: text })}
                            secureTextEntry={true}
                            placeholder="Password" />
                    </View>
                    <View>
                        <TouchableHighlight
                            onPress={this.onRegister}
                            style={{
                                backgroundColor: '#FF0033',
                                marginTop: 10,
                                padding: 5,
                                borderRadius: 12,
                                height: 50,
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                            <Text style={{
                                color: "white",
                                fontSize: 30,
                            }}>Submit</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
}

export default Register
