import React, { Fragment, Component } from 'react';
import {
  AsyncStorage, View
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import LoginScreen from './components/login'
import NewsScreen from './components/news';
import RegisterScreen from './components/register'
import { createStackNavigator, createAppContainer, createDrawerNavigator, createSwitchNavigator, DrawerNavigator, createBottomTabNavigator, navigationOption } from 'react-navigation';
import firebase from "firebase";
import categoryNews from "./components/categoryNews";
import DraweOptions from "./helpers/DrawerOptions";
import AppIntro from "./components/appIntro";
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootReducer from './reducers';

const store = createStore(rootReducer);
store.subscribe(() => console.log('store', store.getState()));

const DrawerMenu = createDrawerNavigator({
  Home: {
    screen: NewsScreen,
    navigationOption: {
      title: "intro"
    }
  },
  Category: {
    screen: categoryNews
  },
}
  , {
    contentComponent: DraweOptions
  }
)

const AuthNavigator = createStackNavigator({
  IntroApp: {
    screen: AppIntro,
    navigationOptions: {
      header: null,
    },
  },

  Login: {
    screen: LoginScreen,
  },

  Register: {
    screen: RegisterScreen,
    navigationOptions: {
      title: 'Register New Account',
      headerStyle: {
        backgroundColor: '#000066',
      },
      headerTintColor: 'white',
    },

  },

});
const AppNavigator = createSwitchNavigator({
  Auth: AuthNavigator,
  App: DrawerMenu,

});

var firebaseConfig = {
  apiKey: "AIzaSyAt5-uaFyk4lvR3IDjN5Dj_xbOw_ZS-dsU",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: ""
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);



const App = createAppContainer(AppNavigator);
export default class MyApp extends React.Component {
  render() {
    return (
    
    <Provider store={store}>
        
          <App />
        
      </Provider>
    )
  }
}
