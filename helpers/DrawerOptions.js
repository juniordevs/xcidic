import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    TextInput,
    Text,
    Modal,
    Alert,
    TouchableOpacity,
    TouchableHighlight,
    ProgressBarAndroid,
    SectionList,
    AsyncStorage,
    StatusBar,
    ImageBackground,
    navigator,
    Image

} from 'react-native';
import {

    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { Container, List, Content, ListItem, Switch, Spinner, Header, Title, Button, Icon, Left, Right, Body } from "native-base";
import { colors } from 'react-native-elements';
import firebase from "firebase";

class DrawerMenuOption extends React.Component {
    onLogOut = async () => {
        try {
            firebase.auth().signOut().then(async user => {
                console.log(user);
                Alert.alert(
                    'Sign Out',
                    'Press OK to Sign out or cancel to change your mind',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                        {
                            text: 'OK', onPress: async () => {
                                await AsyncStorage.removeItem("currentUser");
                                this.props.navigation.navigate('Login');
                            }
                        },
                    ],
                    { cancelable: false },
                );
            })
        }
        catch (err) {
            Alert.alert(err);
        }

    }
    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: "#FF0033" }}>
                    <Body><Text style={{ color: "white", fontSize: 23 }}>News App</Text></Body>
                    <Right>
                    <Button icon transparent onPress={() => this.props.navigation.closeDrawer()}>
                    <Icon type="FontAwesome" name="arrow-left" />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <ListItem itemDivider style={{ height: 60 }}>
                        <Text style={{ fontSize: 20 }}>Pick Your Favorite Category</Text>
                    </ListItem>

                    <ListItem icon style={{ height: 50 }} onPress={async () => {
                        this.props.navigation.navigate('Home')
                        await this.props.navigation.closeDrawer();
                    }} >
                        <Left>
                            
                        </Left>
                        <Body>
                            <Text style={{ fontSize: 19 }}>General</Text>
                        </Body>
                    </ListItem>

                    <ListItem icon style={{ height: 50 }} onPress={async () => {
                        this.props.navigation.navigate('Category', {
                            categoryName: "sports",
                            headerTitle: "Sports"
                        })
                        await this.props.navigation.closeDrawer();
                    }} >
                        <Left>
                            
                        </Left>
                        <Body>
                            <Text style={{ fontSize: 19 }}>Sports</Text>
                        </Body>
                    </ListItem>

                    <ListItem icon style={{ height: 50 }} onPress={async () => {
                        this.props.navigation.navigate('Category', {
                            categoryName: "business",
                            headerTitle: "Business"
                        })
                        await this.props.navigation.closeDrawer();
                    }}>
                        <Left>
                            
                        </Left>
                        <Body>
                            <Text style={{ fontSize: 19 }}>Business</Text>
                        </Body>
                    </ListItem >

                    <ListItem icon style={{ height: 50 }} onPress={async () => {
                        this.props.navigation.navigate('Category', {
                            categoryName: "entertainment",
                            headerTitle: "Entertainment"
                        })
                        await this.props.navigation.closeDrawer();
                    }}>
                        <Left>
                            
                        </Left>
                        <Body>
                            <Text style={{ fontSize: 19 }}>Entertainment</Text>
                        </Body>
                    </ListItem>

                    <ListItem icon style={{ height: 50 }} onPress={async () => {
                        this.props.navigation.navigate('Category', {
                            categoryName: "health",
                            headerTitle: "Health"
                        })
                        await this.props.navigation.closeDrawer();
                    }}>
                        <Left>
                            
                        </Left>
                        <Body>
                            <Text style={{ fontSize: 19 }}>Health</Text>
                        </Body>
                    </ListItem>

                    
                    <ListItem itemDivider style={{ height: 60 }}>
                        <Text style={{ fontSize: 20 }} >Account</Text>
                    </ListItem>
                    <ListItem icon onPress={this.onLogOut} style={{ height: 50 }}>
                        <Left>
                            
                        </Left>
                        <Body>
                            <Text style={{ fontSize: 19 }}>Logout</Text>
                        </Body>
                    </ListItem>
                </Content>
            </Container>



        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    logoutButton: {
        bottom: 0,
        alignItems: "center"
    }
});



export default DrawerMenuOption