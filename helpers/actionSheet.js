import { Container, Header, Title, ActionSheet, Button, Icon, Left, Right, Body, Item, Picker } from "native-base";
import { Alert } from "react-native";
var BUTTONS = [
    { text: "Option 0", icon: "american-football", iconColor: "#2c8ef4" },
    { text: "Option 1", icon: "analytics", iconColor: "#f42ced" },
    { text: "Option 2", icon: "aperture", iconColor: "#ea943b" },
    { text: "Delete", icon: "trash", iconColor: "#fa213b" },
    { text: "Cancel", icon: "close", iconColor: "#25de5b" }
];

var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

const actionScheet = () => {
    ActionSheet.show(
        {
            options: BUTTONS,
            cancelButtonIndex: CANCEL_INDEX,
            destructiveButtonIndex: DESTRUCTIVE_INDEX,
            title: "Testing ActionSheet"
        },
        buttonIndex => {
            Alert.alert("hllo")
        }
    )

}
export default actionScheet;